# Cuestionario SQL



## Objetivo

Afianzar conceptos principales de SQL y estructurar queries básicas. Para completar, crear un branch propio e ir agregando las respuestas a las preguntas en el readme. Los ejercicios para escribir queries toman como base el proyecto en el que trabajamos.



## Preguntas y ejercicios

- ¿Que es SQL?
- ¿Qué es una tabla?
- ¿Qué es una columna?
- ¿Cuáles son los tipos de datos principales?
- ¿Cuál es la diferencia entre SQL y un gestor de base de datos (MySQL, SQL Server, etc)?
- ¿Cómo se relacionan las tablas en una base de datos?
- Escribir una query que liste todos los usuarios con los datos de la ONG a la que pertenecen (tomando como base los modelos existentes de User y Organization en el proyecto)
- ¿Qué es un Job en SQL Server?
- ¿Que es una store procedure en SQLServer?
- Crear una store procedure para eliminar todos los usuarios de una ONG en base al id de la misma.



